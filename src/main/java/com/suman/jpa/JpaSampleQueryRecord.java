package com.suman.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import com.suman.jpa.entity.BookEntity;

public class JpaSampleQueryRecord {

	public static void main(String[] args) {
		EntityManagerFactory emf = null;
		EntityManager em = null;

		try {
			emf = Persistence.createEntityManagerFactory("MySqlUnit");
			em = emf.createEntityManager();
			System.out.println("Connection established");
			TypedQuery<BookEntity> query = em.createQuery("select b from BookEntity b", BookEntity.class);
			List<BookEntity> results = query.getResultList();
			results.forEach(book -> {
				System.out.println(book);
			});
		} catch (

		Exception e) {
			e.printStackTrace();

		} finally {
			if (em != null) {
				em.close();
			}
			if (emf != null) {
				emf.close();
			}
		}
	}

}
