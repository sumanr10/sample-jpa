package com.suman.jpa.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import jdk.jfr.BooleanFlag;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "book")
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@NamedQueries({ @NamedQuery(name = QueryConstant.Q_BOOK_SELECTALL, query = "select b from BookEntity b"),
		@NamedQuery(name = QueryConstant.Q_BOOK_SEARCH, query = "select b from BookEntity b where b.bookName like :bName"),
		@NamedQuery(name = QueryConstant.Q_BOOK_COUNT, query = "select count(b) from BookEntity b"),
		@NamedQuery(name = QueryConstant.Q_BOOK_SELECT_FIELDS, query = "select b.bookId,b.bookName from BookEntity b") })
@NamedNativeQuery(name = QueryConstant.NQ_BOOK_SELECT_ALL, query = "select * from book b", resultClass = BookEntity.class)
@EntityListeners({ LogListener.class })
// cannot use *, b is an alias

public class BookEntity {

	@Id
	@Column(name = "bookId")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long bookId;
	@Column(name = "bookName")
	private String bookName;
	@Column(name = "bookAuthor")
	private String author;
	@Column(name = "publishedYear")
	private Integer publishedYear;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "book")

	private BookDetailEntity detail;

	@Builder.Default
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "book")
	private List<BookOrderEntity> orders = new ArrayList<>();

}
