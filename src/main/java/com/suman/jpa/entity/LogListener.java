package com.suman.jpa.entity;

import javax.persistence.PostPersist;
import javax.persistence.PrePersist;

public class LogListener {

	@PrePersist
	public void beforeInsert(BookEntity entity) {
		System.out.println("Before insert: " + entity);
	}

	@PostPersist
	public void afterInsert(BookEntity entity) {
		System.out.println("After insert: " + entity);
	}
	
}
