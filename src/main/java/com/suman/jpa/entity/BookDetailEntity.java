package com.suman.jpa.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table(name = "book_details")
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude = "bpok")

public class BookDetailEntity {

	@Id
	@Column(name = "id")
	@GenericGenerator(name = "bookIdGen", strategy = "foreign", parameters = {
			@Parameter(name = "property", value = "book") })
	@GeneratedValue(generator = "bookIdGen")
	private Long pk;
	@Column(name = "detail")
	private String detail;

	@OneToOne(fetch = FetchType.LAZY)
	@PrimaryKeyJoinColumn
	private BookEntity book;

}
