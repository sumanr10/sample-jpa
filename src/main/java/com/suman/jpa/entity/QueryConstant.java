package com.suman.jpa.entity;

public interface QueryConstant {

	String Q_BOOK_SELECTALL = "Q_BOOK_SELECTALL";
	String Q_BOOK_SEARCH = "Q_BOOK_SEARCH";
	String Q_BOOK_COUNT = "Q_BOOK_COUNT";
	String Q_BOOK_SELECT_FIELDS = "Q_BOOK_SELECT_FIELDS";
	String NQ_BOOK_SELECT_ALL = "NQ_BOOK_SELECT_ALL";

}
