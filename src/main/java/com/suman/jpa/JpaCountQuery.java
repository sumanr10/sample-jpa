package com.suman.jpa;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.suman.jpa.entity.QueryConstant;

public class JpaCountQuery {

	public static void main(String[] args) {
		EntityManagerFactory emf = null;
		EntityManager em = null;

		try {
			emf = Persistence.createEntityManagerFactory("MySqlUnit");
			em = emf.createEntityManager();
			System.out.println("Connection established");
			Long count = (Long) em.createNamedQuery(QueryConstant.Q_BOOK_COUNT).getSingleResult();
			System.out.println("count : " + count);
		} catch (

		Exception e) {
			e.printStackTrace();

		} finally {
			if (em != null) {
				em.close();
			}
			if (emf != null) {
				emf.close();
			}
		}
	}

}
