package com.suman.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import com.suman.jpa.entity.BookEntity;
import com.suman.jpa.entity.QueryConstant;

public class JpaNamedNativeQuery {

	public static void main(String[] args) {
		EntityManagerFactory emf = null;
		EntityManager em = null;

		try {
			emf = Persistence.createEntityManagerFactory("MySqlUnit");
			em = emf.createEntityManager();
			System.out.println("Connection established");
			final TypedQuery<BookEntity> query = em.createNamedQuery(QueryConstant.NQ_BOOK_SELECT_ALL,
					BookEntity.class);
			List<BookEntity> result = query.getResultList();

			result.forEach(book -> {
				System.out.println(book);
			});
		} catch (

		Exception e) {
			e.printStackTrace();

		} finally {
			if (em != null) {
				em.close();
			}
			if (emf != null) {
				emf.close();
			}
		}
	}

}
