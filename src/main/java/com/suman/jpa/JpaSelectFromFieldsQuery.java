package com.suman.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.suman.jpa.entity.BookEntity;
import com.suman.jpa.entity.QueryConstant;

public class JpaSelectFromFieldsQuery {

	public static void main(String[] args) {
		EntityManagerFactory emf = null;
		EntityManager em = null;

		try {
			emf = Persistence.createEntityManagerFactory("MySqlUnit");
			em = emf.createEntityManager();
			System.out.println("Connection established");
			final Query query = em.createNamedQuery(QueryConstant.Q_BOOK_SELECT_FIELDS);
			List<Object[]> result = query.getResultList();
			result.forEach(row -> {
				System.out.println(row[0] + "==>" + row[1]);
			});
		} catch (

		Exception e) {
			e.printStackTrace();

		} finally {
			if (em != null) {
				em.close();
			}
			if (emf != null) {
				emf.close();
			}
		}
	}

}
