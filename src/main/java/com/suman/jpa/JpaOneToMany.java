package com.suman.jpa;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import com.suman.jpa.entity.BookDetailEntity;
import com.suman.jpa.entity.BookEntity;
import com.suman.jpa.entity.BookOrderEntity;

public class JpaOneTomany {

	public static void main(String[] args) {
		EntityManagerFactory emf = null;
		EntityManager em = null;
		EntityTransaction et = null;

		try {
			emf = Persistence.createEntityManagerFactory("MySqlUnit");
			em = emf.createEntityManager();
			System.out.println("Connection established");
			et = em.getTransaction();
			et.begin();

			// All the db transaction goes here

			BookEntity book = BookEntity.builder().bookName("Pani ko ghaam").author("Amar Neupane").build();

			BookOrderEntity order1 = BookOrderEntity.builder().book(book).orderNumber("ORD-00001").build();
			BookOrderEntity order2 = BookOrderEntity.builder().book(book).orderNumber("ORD-00002").build();

			book.getOrders().add(order1);
			book.getOrders().add(order2);

			em.persist(book);
			et.commit();

		} catch (Exception e) {
			e.printStackTrace();
			if (et != null) {
				et.setRollbackOnly();
			}

		} finally {
			if (em != null) {
				em.close();
			}
			if (emf != null) {
				emf.close();
			}
		}
	}

}
