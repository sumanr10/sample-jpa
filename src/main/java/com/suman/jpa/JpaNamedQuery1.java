package com.suman.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import com.suman.jpa.entity.BookEntity;
import com.suman.jpa.entity.QueryConstant;

public class JpaNamedQuery1 {

	public static void main(String[] args) {
		EntityManagerFactory emf = null;
		EntityManager em = null;

		try {
			emf = Persistence.createEntityManagerFactory("MySqlUnit");
			em = emf.createEntityManager();
			System.out.println("Connection established");
			final TypedQuery<BookEntity> query = em.createNamedQuery(QueryConstant.Q_BOOK_SELECTALL, BookEntity.class);
			final TypedQuery<BookEntity> query1 = em.createNamedQuery(QueryConstant.Q_BOOK_SEARCH, BookEntity.class);

			query1.setParameter("bName", "%java%");
			final List<BookEntity> results = query.getResultList();
			results.forEach(book -> {
				System.out.println(book);
			});
			final List<BookEntity> result1 = query1.getResultList();
			result1.forEach(book -> {
				System.out.println(book);
			});
		} catch (

		Exception e) {
			e.printStackTrace();

		} finally {
			if (em != null) {
				em.close();
			}
			if (emf != null) {
				emf.close();
			}
		}
	}

}
