package com.suman.jpa;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import com.suman.jpa.entity.BookEntity;

public class JpaCreateRecordExample {

	public static void main(String[] args) {
		EntityManagerFactory emf = null;
		EntityManager em = null;
		EntityTransaction et = null;

		try {
			emf = Persistence.createEntityManagerFactory("MySqlUnit");
			em = emf.createEntityManager();
			System.out.println("Connection established");
			et = em.getTransaction();
			et.begin();

			// All the db transaction goes here

			BookEntity entity = BookEntity.builder().bookName("Seto Bagh").author("Diamond Shamsher").build();

			em.persist(entity);
			et.commit();
//			BookEntity entity = new BookEntity();
//			entity.setBookName("Java");
//			entity.setAuthor("Aanand");

		} catch (Exception e) {
			e.printStackTrace();
			if (et != null) {
				et.setRollbackOnly();
			}

		} finally {
			if (em != null) {
				em.close();
			}
			if (emf != null) {
				emf.close();
			}
		}
	}

}
